<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

   <?= Html::a('<span class="logo-mini">SPC</span><span class="logo-lg">ShoppingPC</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">


                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $directoryAsset ?>/img/sidebar_usuario-corporativo.png" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= $directoryAsset ?>/img/sidebar_usuario-corporativo.png" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                <small>Bienvenido</small>
                                <?= Yii::$app->user->identity->username ?>
                            </p>
                        </li>
               
                        <li class="user-footer">
                            
                            <div class="pull-right">
                                <?= Html::a(
                                    'Cerrar Sesion',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

              
            </ul>
        </div>
    </nav>
</header>
