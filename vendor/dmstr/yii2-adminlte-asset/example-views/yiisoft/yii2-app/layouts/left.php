<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/sidebar_usuario-corporativo.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>



        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Inicio', 'icon' => 'th-large', 'url' => ['/site/index']],
                    [
                        'label' => 'Administración',
                        'icon' => 'users',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Usuarios', 'icon' => 'user-plus', 'url' => ['/user/admin'],],
                            ['label' => 'Asignaciones', 'icon' => 'exchange', 'url' => ['/admin/assignment'],],
                            ['label' => 'Rutas', 'icon' => 'list', 'url' => ['/admin/route'],],
                            ['label' => 'Roles', 'icon' => 'unlock-alt', 'url' => ['/admin/role'],],

                        ],

                    ],
                    [
                        'label' => 'Registros',
                        'icon' => 'edit',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Sucursal', 'icon' => 'circle', 'url' => ['/sucursal'],],
                            ['label' => 'cliente', 'icon' => 'user-plus', 'url' => ['/cliente'],],
                            ['label' => 'Tipo de producto', 'icon' => 'list', 'url' => ['/tipo_producto'],],
                            ['label' => 'Proveedor', 'icon' => 'share', 'url' => ['/proveedor'],],
                            ['label' => 'Lote', 'icon' => 'square', 'url' => ['/lote'],],
                            ['label' => 'Almacen', 'icon' => 'tasks', 'url' => ['/almacen'],],
                            ['label' => 'Sueldos y salarios', 'icon' => 'user', 'url' => ['/sueldos_y_salarios'],],
                        ],

                    ],




                    //['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    //['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    //['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>
