<?php
    /*
     * This file is part of the Dektrium project.
     *
     * (c) Dektrium project <http://github.com/dektrium>
     *
     * For the full copyright and license information, please view the LICENSE.md
     * file that was distributed with this source code.
     */
    
    use dektrium\user\widgets\Connect;
    use dektrium\user\models\LoginForm;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    
    /**
     * @var yii\web\View $this
     * @var dektrium\user\models\LoginForm $model
     * @var dektrium\user\Module $module
     */
    
    $this->title = Yii::t('user', 'Sign in');
    $this->params['breadcrumbs'][] = $this->title;
    ?>
<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>
<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>Shopping</b> PC</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Iniciar Sesión</p>
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'validateOnBlur' => false,
            'validateOnType' => false,
            'validateOnChange' => false,
            ]) ?>
        <?php if ($module->debug): ?>
        <?= $form->field($model, 'login', [
            'inputOptions' => [
                'autofocus' => 'autofocus',
                'class' => 'form-control',
                'tabindex' => '1']])->dropDownList(LoginForm::loginList());
            ?>
        <?php else: ?>
        <?= $form->field($model, 'login',
            ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']]
            );
            ?>
        <?php endif ?>
        <?php if ($module->debug): ?>
        <div class="alert alert-warning">
            <?= Yii::t('user', 'Password is not necessary because the module is in DEBUG mode.'); ?>
        </div>
        <?php else: ?>
        <?= $form->field($model, 'password', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])->passwordInput()->label(Yii::t('user', 'Password') . ($module->enablePasswordRecovery ?'' : '')) ?>
        <?php endif ?>
        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '3']) ?>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton(
                    Yii::t('user', 'Ingresar'),
                    ['class' => 'btn btn-primary btn-block btn-flat', 'tabindex' => '4']
                    ) ?>
            </div>
            <!-- /.col -->
        </div>
        <?php ActiveForm::end(); ?>
        <div class="social-auth-links text-center">
            <p>- O -</p>
            <a href="#" class="btn btn-block btn-social btn-vk"><i class="fa fa-fw fa-globe"></i> Portal WEB Shopping PC</a>
        </div>
        <!-- /.social-auth-links -->
        <br>
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->