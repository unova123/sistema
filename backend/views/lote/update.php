<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Lote */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Lote',
]) . $model->id_lote;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lotes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_lote, 'url' => ['view', 'id' => $model->id_lote]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="lote-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
