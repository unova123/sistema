<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Lote */

$this->title = Yii::t('app', 'Create Lote');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lotes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lote-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
