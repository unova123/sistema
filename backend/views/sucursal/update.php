<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Sucursal */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Sucursal',
]) . $model->id_sucursal;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sucursals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_sucursal, 'url' => ['view', 'id' => $model->id_sucursal]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="sucursal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
