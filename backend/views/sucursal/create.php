<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Sucursal */

$this->title = Yii::t('app', 'Create Sucursal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sucursals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sucursal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
