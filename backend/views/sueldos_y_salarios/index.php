<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\Sueldos_y_salariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sueldos Y Salarios');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sueldos-y-salarios-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Sueldos Y Salarios'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_sueldos_y_salarios',
            'nombre_completo',
            'fecha_nacimiento',
            'telefono',
            'celular',
            // 'direccion',
            // 'email:email',
            // 'fecha_inicio',
            // 'fecha_fin',
            // 'contrato',
            // 'salario',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
