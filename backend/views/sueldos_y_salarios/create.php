<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Sueldos_y_salarios */

$this->title = Yii::t('app', 'Create Sueldos Y Salarios');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sueldos Y Salarios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sueldos-y-salarios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
