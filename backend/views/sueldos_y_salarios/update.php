<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Sueldos_y_salarios */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Sueldos Y Salarios',
]) . $model->id_sueldos_y_salarios;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sueldos Y Salarios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_sueldos_y_salarios, 'url' => ['view', 'id' => $model->id_sueldos_y_salarios]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="sueldos-y-salarios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
