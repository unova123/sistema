<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cliente".
 *
 * @property integer $id_cliente
 * @property string $nombre_completo
 * @property integer $nit_ci
 * @property string $razon_social
 * @property integer $telefono
 * @property integer $celular
 * @property string $email
 * @property string $password
 * @property string $direccion
 *
 * @property Cotizacion[] $cotizacions
 * @property Venta[] $ventas
 */
class Cliente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nit_ci', 'telefono', 'celular'], 'integer'],
            [['nombre_completo', 'razon_social', 'email', 'password', 'direccion'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_cliente' => Yii::t('app', 'Id Cliente'),
            'nombre_completo' => Yii::t('app', 'Nombre Completo'),
            'nit_ci' => Yii::t('app', 'Nit Ci'),
            'razon_social' => Yii::t('app', 'Razon Social'),
            'telefono' => Yii::t('app', 'Telefono'),
            'celular' => Yii::t('app', 'Celular'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'direccion' => Yii::t('app', 'Direccion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCotizacions()
    {
        return $this->hasMany(Cotizacion::className(), ['id_cliente' => 'id_cliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Venta::className(), ['id_cliente' => 'id_cliente']);
    }
}
