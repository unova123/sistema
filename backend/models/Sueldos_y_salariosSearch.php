<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Sueldos_y_salarios;

/**
 * Sueldos_y_salariosSearch represents the model behind the search form about `backend\models\Sueldos_y_salarios`.
 */
class Sueldos_y_salariosSearch extends Sueldos_y_salarios
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_sueldos_y_salarios', 'telefono', 'celular'], 'integer'],
            [['nombre_completo', 'fecha_nacimiento', 'direccion', 'email', 'fecha_inicio', 'fecha_fin', 'contrato'], 'safe'],
            [['salario'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sueldos_y_salarios::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_sueldos_y_salarios' => $this->id_sueldos_y_salarios,
            'fecha_nacimiento' => $this->fecha_nacimiento,
            'telefono' => $this->telefono,
            'celular' => $this->celular,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_fin' => $this->fecha_fin,
            'salario' => $this->salario,
        ]);

        $query->andFilterWhere(['like', 'nombre_completo', $this->nombre_completo])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'contrato', $this->contrato]);

        return $dataProvider;
    }
}
