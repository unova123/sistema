<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tipo_producto".
 *
 * @property integer $id_tipo_producto
 * @property string $nombre
 *
 * @property Producto[] $productos
 */
class Tipo_producto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_producto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tipo_producto' => Yii::t('app', 'Id Tipo Producto'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Producto::className(), ['id_tipo_producto' => 'id_tipo_producto']);
    }
}
