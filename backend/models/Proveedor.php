<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "proveedor".
 *
 * @property integer $id_proveedor
 * @property string $nombre_completo
 * @property string $telefono
 * @property string $celular
 * @property string $email
 *
 * @property Compra[] $compras
 * @property CuentaBancaria[] $cuentaBancarias
 */
class Proveedor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proveedor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre_completo', 'telefono', 'celular', 'email'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_proveedor' => Yii::t('app', 'Id Proveedor'),
            'nombre_completo' => Yii::t('app', 'Nombre Completo'),
            'telefono' => Yii::t('app', 'Telefono'),
            'celular' => Yii::t('app', 'Celular'),
            'email' => Yii::t('app', 'Email'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->hasMany(Compra::className(), ['id_proveedor' => 'id_proveedor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuentaBancarias()
    {
        return $this->hasMany(CuentaBancaria::className(), ['id_proveedor' => 'id_proveedor']);
    }
}
