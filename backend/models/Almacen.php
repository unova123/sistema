<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "almacen".
 *
 * @property integer $id_almacen
 * @property string $nombre
 *
 * @property DetalleCompra[] $detalleCompras
 */
class Almacen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'almacen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_almacen' => Yii::t('app', 'Id Almacen'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleCompras()
    {
        return $this->hasMany(DetalleCompra::className(), ['id_almacen' => 'id_almacen']);
    }
}
