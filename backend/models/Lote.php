<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "lote".
 *
 * @property integer $id_lote
 * @property string $nombre
 *
 * @property DetalleCompra[] $detalleCompras
 */
class Lote extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lote';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_lote' => Yii::t('app', 'Id Lote'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleCompras()
    {
        return $this->hasMany(DetalleCompra::className(), ['id_lote' => 'id_lote']);
    }
}
