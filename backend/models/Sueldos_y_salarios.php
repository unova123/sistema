<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sueldos_y_salarios".
 *
 * @property integer $id_sueldos_y_salarios
 * @property string $nombre_completo
 * @property string $fecha_nacimiento
 * @property integer $telefono
 * @property integer $celular
 * @property string $direccion
 * @property string $email
 * @property string $fecha_inicio
 * @property string $fecha_fin
 * @property string $contrato
 * @property string $salario
 */
class Sueldos_y_salarios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sueldos_y_salarios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre_completo', 'fecha_nacimiento', 'telefono', 'celular', 'direccion', 'email', 'fecha_inicio', 'fecha_fin', 'contrato', 'salario'], 'required'],
            [['fecha_nacimiento', 'fecha_inicio', 'fecha_fin'], 'safe'],
            [['telefono', 'celular'], 'integer'],
            [['salario'], 'number'],
            [['nombre_completo', 'contrato'], 'string', 'max' => 45],
            [['direccion', 'email'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_sueldos_y_salarios' => Yii::t('app', 'Id Sueldos Y Salarios'),
            'nombre_completo' => Yii::t('app', 'Nombre Completo'),
            'fecha_nacimiento' => Yii::t('app', 'Fecha Nacimiento'),
            'telefono' => Yii::t('app', 'Telefono'),
            'celular' => Yii::t('app', 'Celular'),
            'direccion' => Yii::t('app', 'Direccion'),
            'email' => Yii::t('app', 'Email'),
            'fecha_inicio' => Yii::t('app', 'Fecha Inicio'),
            'fecha_fin' => Yii::t('app', 'Fecha Fin'),
            'contrato' => Yii::t('app', 'Contrato'),
            'salario' => Yii::t('app', 'Salario'),
        ];
    }
}
